package ai.robot.rest.memory;

import java.util.concurrent.ConcurrentHashMap;

public class MemoryStorage {
	static ConcurrentHashMap<String, String> incomingStore = new ConcurrentHashMap<String, String>();
	static boolean serverIsActive;
	
	public static ConcurrentHashMap<String, String> getIncomingStore() {
		return incomingStore;
	}

	public static void setIncomingStore(ConcurrentHashMap<String, String> incomingStore) {
		MemoryStorage.incomingStore = incomingStore;
	}

	public static synchronized boolean isServerIsActive() {
		return serverIsActive;
	}

	public static void setServerIsActive(boolean serverIsActive) {
		MemoryStorage.serverIsActive = serverIsActive;
	}

}
