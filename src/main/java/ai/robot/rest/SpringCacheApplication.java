package ai.robot.rest;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;




@SpringBootApplication 
@EnableScheduling
public class SpringCacheApplication {
	
	public static void main(String[] args) throws IOException {
	
		
		SpringApplication.run(SpringCacheApplication.class, args);
   }

}
