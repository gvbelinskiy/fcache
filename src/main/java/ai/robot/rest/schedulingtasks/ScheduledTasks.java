package ai.robot.rest.schedulingtasks;

import java.time.Clock;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ai.robot.rest.memory.MemoryStorage;
import ai.robot.rest.service.IClientService;


@Component
public class ScheduledTasks {
	@Autowired  
	private IClientService clientService;  
	
	
	@Scheduled(fixedDelay = 5000)
	public void reportToLive() {
			try {
				
				ConcurrentHashMap<String, String> stored = MemoryStorage.getIncomingStore();
				ConcurrentHashMap<String, String> incoming = new ConcurrentHashMap<>(stored);
				stored.clear();
			
				
				for (Map.Entry<String, String> entryMap : incoming.entrySet()) {
					try {
							String deviceId=entryMap.getKey();
							String json=entryMap.getValue();
							boolean status=clientService.sendToServer(deviceId, json);
							if(!status) {
									Instant instant2 = Clock.systemUTC().instant();
									long now = instant2.toEpochMilli();
									MemoryStorage.getIncomingStore().put(deviceId+"_"+now, json);
							}
							
						} catch (Exception e) {
								e.printStackTrace();
						}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	@Scheduled(fixedDelay = 1000)
	public void testService() {
		RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        String deviceData = "{\"somekey\":\"somedata\"}";
        String deviceId = UUID.randomUUID().toString();
        
        Map<String, Object> map = new HashMap<>();
	    map.put("deviceId", deviceId);
	    map.put("json", deviceData);
	 	HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        String oneweb="http://localhost:8080/api/start/"+deviceId; 
    	
	    restTemplate.postForEntity(oneweb, entity, String.class);
	}
	
	@Scheduled(fixedDelay = 3000)
	public void testServerInError() {
		MemoryStorage.setServerIsActive(false);
	}
	
	@Scheduled(fixedDelay = 2000)
	public void testServerIsUp() {
		MemoryStorage.setServerIsActive(true);
	}
	
}
