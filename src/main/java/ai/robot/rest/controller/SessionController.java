package ai.robot.rest.controller;

import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import ai.robot.rest.memory.MemoryStorage;
import ai.robot.rest.service.IClientService;

@RestController
@RequestMapping(path = "/api")
public class SessionController {
	
	@Autowired  
	private IClientService clientService;  
	
	@RequestMapping(value = "start/{deviceId}", method = RequestMethod.POST)
	public ResponseEntity<String> handleSession( @PathVariable String deviceId, @RequestBody String json) throws IOException {
		try {
			if (deviceId != null && deviceId != "" && json != null && json != "") {
				if(!MemoryStorage.isServerIsActive()) {
					
							Instant instant2 = Clock.systemUTC().instant();
							long now = instant2.toEpochMilli();
							MemoryStorage.getIncomingStore().put(deviceId+"_"+now, json);
				
				}else{
					boolean status=clientService.sendToServer(deviceId, json);
					if(!status) {
							Instant instant2 = Clock.systemUTC().instant();
							long now = instant2.toEpochMilli();
							MemoryStorage.getIncomingStore().put(deviceId+"_"+now, json);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		String dsv = "{\"done\":\"yes\"}";
		return new ResponseEntity<String>(dsv, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value = "save/{deviceId}", method = RequestMethod.POST)
	public ResponseEntity<String> serverSession( @PathVariable String deviceId, @RequestBody String json) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		String dsv = "{\"done\":\"yes\"}";
		System.out.println("saved "+deviceId);
		
		return new ResponseEntity<String>(dsv, headers, HttpStatus.OK);
	}
}
