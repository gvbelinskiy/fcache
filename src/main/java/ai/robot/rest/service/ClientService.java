package ai.robot.rest.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ai.robot.rest.memory.MemoryStorage;

@Service  
public class ClientService implements IClientService {
		
		@Override  
		public boolean sendToServer(String deviceId, String json){
			boolean status=false;
			MemoryStorage.setServerIsActive(false);
			RestTemplate restTemplate = new RestTemplate();
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        
	        Map<String, Object> map = new HashMap<>();
		    map.put("deviceId", deviceId);
		    map.put("json", json);
		    
		 	HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
		 	
	     
	        String oneweb="http://localhost:8080/api/save/"+deviceId; 
	    	try {
		        ResponseEntity<String> response = restTemplate.postForEntity(oneweb, entity, String.class);
		        if(response.getStatusCode()==HttpStatus.OK) {
		        	status=true;
		        	MemoryStorage.setServerIsActive(true);
		        }else {
		        	MemoryStorage.setServerIsActive(false);
		        }
			
			} catch (Exception e) {
				e.printStackTrace();
				MemoryStorage.setServerIsActive(false);
			}
	    	
	    	return status;
	}
}
