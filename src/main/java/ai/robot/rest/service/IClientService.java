package ai.robot.rest.service;

public interface IClientService {
  boolean sendToServer(String deviceId, String json);
}
